import { mnemonicToSeedSync } from "bip39";
import NexCore from "nexcore-lib";
import HDPrivateKey from "nexcore-lib/types/lib/hdprivatekey";
import { AddressKey, WalletKeys } from "src/interfaces/wallet-keys.interface";

export enum DerivePath {
    RECEIVE = 0,
    CHANGE = 1
}

export class WalletHelper {

    public static generateAccountKey(mnemonic: string, accountIndex: number = 0) {
        const seed = mnemonicToSeedSync(mnemonic, undefined);
        const masterKey = NexCore.HDPrivateKey.fromSeed(seed);
        return masterKey.deriveChild(44, true).deriveChild(29223, true).deriveChild(accountIndex, true);
    }

    public static generateKeysAndAddresses(accountKey: HDPrivateKey, fromRIndex: number, rIndex: number, fromCIndex: number, cIndex: number): WalletKeys {
        let receive = accountKey.deriveChild(0, false);
        let change = accountKey.deriveChild(1, false);
        let rKeys: AddressKey[] = [], cKeys: AddressKey[] = [];
        for (let index = fromRIndex; index < rIndex; index++) {
            let k = receive.deriveChild(index, false);
            let addr = k.getPublicKey().toAddress().toString();
            rKeys.push({key: k, address: addr});
        }
        for (let index = fromCIndex; index < cIndex; index++) {
            let k = change.deriveChild(index, false);
            let addr = k.getPublicKey().toAddress().toString();
            cKeys.push({key: k, address: addr});
        }
        return {receiveKeys: rKeys, changeKeys: cKeys};
    }

    public static generateKeyAndAddress(accountKey: HDPrivateKey, path: DerivePath, index: number): AddressKey {
        let key = accountKey.deriveChild(path, false).deriveChild(index, false);
        let address = key.getPublicKey().toAddress().toString();
        return { key: key, address: address };
    }
}