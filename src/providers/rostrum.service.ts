import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ElectrumClient } from '@vgrunner/electrum-cash';
import { Balance } from 'src/interfaces/balance.interface';
import { BlockTip } from 'src/interfaces/block-tip.interface';
import { UTXO, UnspentUTXO } from 'src/interfaces/utxo.interface';

type RPCParameter = string | number | boolean | null;

@Injectable()
export class RostrumService {

  private readonly logger = new Logger(RostrumService.name);

  private readonly host: string;
  private readonly port: number;

  private readonly client: ElectrumClient;
  
  public constructor(private configService: ConfigService) {
    this.host = configService.get("ROSTRUM_HOST");
    this.port = configService.get("ROSTRUM_PORT");
    this.client = new ElectrumClient('com.token.tool', '1.4.3', this.host, this.port, 'wss', undefined, undefined, true);
  }

  public async initConnection() {
    this.logger.log(`Init connection to rostrum at: ${this.host}:${this.port}`);
    try {
      return await this.client.connect();
    } catch (e) {
      this.logger.error("Failed to connecto to Rostrum instance");
      throw e;
    }
  }

  public async getBalance(address: string) {
    return await this.execute<Balance>('blockchain.address.get_balance', address);
  }

  public async getBlockTip() {
    return await this.execute<BlockTip>('blockchain.headers.tip');
  }

  public async listUnspent(address: string) {
    return await this.execute<UnspentUTXO[]>('blockchain.address.listunspent', address);
  }

  public async getUtxo(outpoint: string) {
    return await this.execute<UTXO>('blockchain.utxo.get', outpoint);
  }

  public async broadcastTransaction(txHex: string) {
    return await this.execute<string>('blockchain.transaction.broadcast', txHex);
  }

  private async execute<T>(method: string, ...parameters: RPCParameter[]) {
    var res = await this.client.request(method, ...parameters);
    if (res instanceof Error) {
        throw res;
    }
    return res as T;
  }
}
