import { MigrationInterface, QueryRunner } from "typeorm";

export class InitDb1690475390730 implements MigrationInterface {
    name = 'InitDb1690475390730'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "wallet_index" (
                "id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                "receiveIndex" integer NOT NULL,
                "changeIndex" integer NOT NULL,
                "tokenIndex" integer NOT NULL
            )`
        );
        await queryRunner.query("INSERT INTO wallet_index VALUES (1, 1, 1, 1)");

        await queryRunner.query(
            `CREATE TABLE "token_tx_state" (
                "id" integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                "uuid" varchar NOT NULL,
                "createTxId" varchar NOT NULL,
                "createTxIdem" varchar NOT NULL,
                "mintTxId" varchar NOT NULL,
                "mintTxIdem" varchar NOT NULL,
                "tokenName" varchar NOT NULL,
                "tokenTicker" varchar NOT NULL,
                "tokenDocUrl" varchar,
                "tokenDocHash" varchar,
                "tokenDocSignature" varchar,
                "tokenDecimals" integer,
                "tokenQuantity" varchar,
                "tokenId" varchar,
                "burnAuth" boolean NOT NULL,
                "destAddress" varchar NOT NULL,
                "payAddress" varchar NOT NULL,
                "status" varchar NOT NULL DEFAULT ('Pending'),
                "date" datetime NOT NULL DEFAULT (datetime('now')),
                CONSTRAINT "UQ_2f576ab51dd7cb4306612666d0b" UNIQUE ("uuid"),
                CONSTRAINT "UQ_fc279b97cdb8807be2076cc50e3" UNIQUE ("createTxId"),
                CONSTRAINT "UQ_51af70a2ddda6cb6345954baca6" UNIQUE ("createTxIdem"),
                CONSTRAINT "UQ_72fd962f58fecfdfad11224cd51" UNIQUE ("mintTxId"),
                CONSTRAINT "UQ_d7dba73c785e8bf02e1f92e1dc2" UNIQUE ("mintTxIdem")
            )`
        );
        await queryRunner.query(`CREATE INDEX "IDX_1d7d38a6be474474bed7ac2672" ON "token_tx_state" ("status")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "token_tx_state"`);
        await queryRunner.query(`DROP TABLE "wallet_index"`);
    }

}
